#!/bin/bash

# If there are files in /tmp/configmap that are not empty
# (overriden by a ConfigMap) copy them to /discourse/config
if [ -n "$(ls -A /tmp/discourse-configmap)" ]
then
  for f in /tmp/discourse-configmap/*
  do
    if [ -s $f ]
    then
      cp /tmp/discourse-configmap/* /discourse/config/
    fi
  done
fi

# Replace environment variables
echo "--> Overwritting env variables ..."
envsubst < /tmp/discourse-configmap/discourse.conf > /discourse/config/discourse.conf
echo "--> DONE"

# Always precompile assets
bundle exec rake assets:precompile RAILS_ENV=production
# Migrate DB
bundle exec rake db:create db:migrate RAILS_ENV=production

# Run discourse with Puma
RAILS_ENV=production bundle exec puma -C ./config/puma.rb config.ru